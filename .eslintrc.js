module.exports = {
  root: true,
  parser: 'babel-eslint',
  parserOptions: {
    sourceType: 'module'
  },
  env: {
    browser: true,
    node: true
  },
  extends: 'standard',
  plugins: [
    'html'
  ],
  rules: {
    quotes: ['error', 'single', {allowTemplateLiterals: true}],
    'comma-dangle': ['error', 'always-multiline'],
    'operator-linebreak': ['error', 'before'],
    'space-before-function-paren': [
      'error',
      {'anonymous': 'always', 'named': 'never'}
    ],
    'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0
  },
}
