import * as types from '../mutation-types'

const state = {
  nodes: [],
  edges: [],
}

const mutations = {
  [types.SET_GRAPH](state, {nodes, edges}) {
    state.nodes = nodes
    state.edges = edges
  },
}

export default {
  state,
  mutations,
}
